export default {
    install (Vue) {
        Vue.prototype.$message = function (text, type) {
            this.$bvToast.toast(text, {
                variant: type,
                toaster: 'b-toaster-top-center',
                solid: true
            })
        }
    }
}
