export default  {
    PRODUCTS (state) {
        return state.products;
    },

    DISPLAY_LIST (state) {
        return state.displayList;
    },

    LOADING (state) {
        return state.loading
    },

    CATEGORY (state) {
        return state.category
    },
    SEARCH (state) {
        return state.search
    },
    SORT_TYPE (state) {
        return state.ascending
    },
    ERROR: s => s.error
}
