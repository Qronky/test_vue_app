export default {
    SET_PRODUCTS_TO_STATE (state, products) {
        state.products = products
    },
    SET_DISPLAY_LIST (state) {
        state.displayList = !state.displayList
    },
    SET_LOADING_MODE (state) {
        state.loading = !state.loading
    },
    SET_CATEGORY (state, category) {
        state.category = category
    },
    SET_SEARCH (state, search) {
        state.search = search
    },
    SET_SORT_TYPE (state) {
        state.ascending = !state.ascending
    },
    SET_ERROR (state, error) {
        state.error = error
    },
    CLEAR_ERROR (state) {
        state.error = null
    }
}
