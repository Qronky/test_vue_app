exports.sortBy = (arr, sortBy) => {
    return arr.sort((a, b) => {
        let fa = a[sortBy].toLowerCase(), fb = b[sortBy].toLowerCase()
        if (fa < fb) {
            return -1
        }
        if (fa > fb) {
            return 1
        }
        return 0
    })

}
exports.dateManipulating = (timestamp) => {
    return new Date(Number(timestamp) * 1000).getFullYear().toString()
}
