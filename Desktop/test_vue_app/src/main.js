import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import VueFilterDateFormat from '@vuejs-community/vue-filter-date-format'
import alertPlugin from '@/utils/alerts.plugin'

import { BootstrapVue } from 'bootstrap-vue'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue)
Vue.use(VueFilterDateFormat);
Vue.use(alertPlugin)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
