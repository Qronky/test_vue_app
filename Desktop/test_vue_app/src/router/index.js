import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    meta: {layout: 'main'},
    component: () => import('../views/Home')
  },
  {
    path :'*',
    component: () => import('../views/Error')
  },
  {
    path: '/product/:id',
    name: 'product',
    meta: {layout: 'main'},
    component: () => import('../views/Product')
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
