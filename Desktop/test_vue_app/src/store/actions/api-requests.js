import axios from "axios";

export default {
    async GET_PRODUCTS_FROM_API({commit}) {

        await axios
            .get('db.json')
            .then(response => {
                commit('SET_PRODUCTS_TO_STATE', response.data.results);
            })
            .catch(error => {
                console.log(error.type)
                commit('SET_ERROR', error)
                throw error
            })
    },
    async UPDATE_PRODUCT_TITLE({state, commit}, item) {
        try {
            // API CALL TO UPDATE DB
            // await axios.post('/api/product/' + this.item.imdbID.....

            // Just to emulate some delay
            const delay = ms => new Promise(resolve => setTimeout(resolve, ms));

            let newArray = [...this.state.products]
            const elementsIndex = state.products.findIndex(element => element.imdbID === item.imdbID)
            newArray[elementsIndex] = {...newArray[elementsIndex], Title: item.newTitle}
            commit('SET_PRODUCTS_TO_STATE', newArray);
            await delay(500);

            // return response
        } catch (error) {
            commit('SET_ERROR', error)
            return error

        }

    },
}
