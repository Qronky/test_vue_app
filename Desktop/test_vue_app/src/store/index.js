import Vue from 'vue'
import Vuex from 'vuex'

import commonActions from './actions/actions'
import apiRequests from './actions/api-requests'
import mutations from "./mutations"
import getters from "./getters";

const actions = {...commonActions, ...apiRequests}

Vue.use(Vuex);

let store = new Vuex.Store({
  state: {
    products: [],
    displayList: true,
    loading: false,
    category: '',
    search: '',
    ascending: true,
    error: null
  },
  mutations,
  actions,
  getters,
});

export default store;
